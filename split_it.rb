#!/usr/bin/env ruby

require 'fileutils'
require 'zlib'

File.open('mthesaur.txt', 'r') do |thesaurus|
  i = 0
  while line = thesaurus.gets
    terms = line.chomp.split(',')
    word = terms.first
    crc = Zlib.crc32(word)
    syns = terms[1..-1]

    puts "Processing #{word} (#{crc}).\n\n"

    dir = "out/#{crc % 10}/#{(crc/10) % 10}/#{(crc/100) % 10}/#{(crc/1000) % 10}"
    mods = FileUtils.mkdir_p(dir)

    File.open("#{dir}/#{crc}.txt", 'w') do |synonyms|
      synonyms.puts(word)
      synonyms.puts(syns.join("\n"))
    end
  end
end

puts "Processing complete!"
